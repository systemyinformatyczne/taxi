def admin_index(request):
    pracownik = authenticate(request) #uwierzytelnienie pracownika
    if not pracownik: #sprawdzenie czy pracownik jest zalogowany, 
					  #jeśli nie wypisanie komunikatu błędu i przekierowanie na stronę główną
        messages.warning(
            request,
            'Musisz się najpierw zalogować jako LABI'
        )
        return redirect('index')

    wnioski = Wniosek.objects.all().order_by('-data') #pobranie wniosków posortowanych po dacie malejąco
    if request.method == 'POST': #sprawdzenie metody Http 
        wnioski = Wniosek.objects.all().order_by('data') #pobranie wniosków posortowanych po dacie rosnąco
        if request.POST.get('data-nowe'):
            wnioski = Wniosek.objects.order_by('-data')
        elif request.POST.get('data-stare'):
            wnioski = Wniosek.objects.order_by('data')

	#linijki 12-17 - pobranie wniosków w zależności od wybranej opcji (najnowsze / najstarsze)

        if request.POST.get('zatwierdz'):
            checked = request.POST.getlist('decyzja')
            for pk in checked:
                Historia.objects.create(
                    wniosek_id=pk,
                    status='2',
                    pracownik=pracownik.login,
                )
				#linijki 21-28 pobranie zatwierdzonych wniosków i wpisanie ich do historii
        elif request.POST.get('odrzuc'):
            checked = request.POST.getlist('decyzja')
            for pk in checked:
                Historia.objects.create(
                    wniosek_id=pk,
                    status='5',
                    pracownik=pracownik.login,
                )
				
				#linijki 30-39 pobranie odrzuconych wniosków i wpisanie ich do historii
                wniosek_mail = Wniosek.objects.get(pk=pk)
                pracownik_w = Pracownik.objects.get(pk=wniosek_mail.pracownik.pk)
                historia_w = Historia.objects.filter(wniosek=pk)
                subject = 'SODO: odrzucowno wniosek nr '+str(wniosek_mail.pk)+' w systemie'
                message = 'Odrzucono twój wniosek o numerze '+str(wniosek_mail.pk)+'.\n' \
                    'Do wiadomości dołączono raport z historią wniosku.\n' \
                    'Wiadomość wygenerowana automatycznie.'
                send_addr = wniosek_mail.pracownik.email
                email = EmailMessage(subject, message, 'sodo.uam.test@gmail.com', [send_addr])
                html = render_to_string('PDF_wnioski/wniosek_rap_pdf_wzor.html',
                                        {'wniosek': wniosek_mail, 'pracownik': pracownik_w, 'historia': historia_w})
                out = BytesIO()
                weasyprint.HTML(string=html).write_pdf(out)
                email.attach('raport_wniosek'+str(wniosek_mail.pk)+'.pdf', out.getvalue(), 'application/pdf')
                email.send()
				#wysłanie maila z powiadomieniem o statusie wniosku
            historia = Historia.objects.filter(wniosek=pk)
            return redirect('admin_index')

    to_approve = []
    for wniosek in wnioski:
        obiekt = wniosek.obiekty.all()[0]
        wniosek_labi = find_labi(obiekt.jedn_org.id)
        if wniosek_labi == pracownik:
            historia = Historia.objects.filter(
                wniosek=wniosek.id,
            ).order_by('-data')[0]
            if historia.status == '1':
                to_approve.append(historia)

    context = {
        'pracownik': pracownik,
        'wnioski': wnioski,
        'to_approve': to_approve,
    }
    return render(request, 'wnioski/homepage/homepage.html', context)

#linijki 59 - 75 wypisanie wniosków "do zatwierdzenia" w formie html


def gen_objs_raport_csv(request):
    obiekty = Obiekt.objects.all()
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="raport_obiekty.csv"'
    writer = csv.writer(response)
    writer.writerow(['id', 'nazwa_obiektu', 'typ_id', 'typ', 'jedn_org_id', 'jedn_org', 'opis'])
    for o in obiekty:
        writer.writerow([o.pk, o.nazwa, o.typ.id, o.typ.nazwa, o.jedn_org.id, o.jedn_org.nazwa, o.opis,])
    return response

#linijki 80 - 88 - funkcja generująca plik csv