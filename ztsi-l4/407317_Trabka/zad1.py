import numpy as np # import biblioteki z nadaniem jej aliasu
import time # import biblioteki


class Point: # definiacja klasy
    def __init__(self, x=0, y=0): # konstruktor
        self.x = x
        self.y = y

def timing(f): # definicja funkcji
    def wrap(*args): # definicja funkcji
        time1 = time.time() # zmiennej przypisz wartosc funkcji
        ret = f(*args)
        time2 = time.time() # zmiennej przypisz wartosc funkcji
        print('{:s} function took {:.3f} ms'.format(f.__name__, (time2-time1)*1000.0)) # wypisanie

        return ret # zwrocenie wartosci
    return wrap # zwrocenie wartosci

@timing
def all_paths(n): # definicja funkcji
    print('Working...\n') # wypisanie
    walks = [] # stworzenie pustej listy
    visited = np.empty((n + 1, n + 1)) # stworzenie nowej tablicy
    visited.fill(False) # zapelnienie tablicy wartosciami
    num_points = (n + 1) * (n + 1) # przypisanie wartosci
    current_walk = [] # stworzenie pustej listy
    start = Point() # instancja klasy
    current_walk.append(start) # dopisanie do listy
    visited[0][0] = True # zmiana wartosci elemetu w tablicy
    find_walks(num_points, walks, current_walk, 0, 0, n, visited) # wywolanie funkcji
    return walks # zwrot listy


def find_walks(num_points, walks, current_walk, curr_x, curr_y, n, visited): # definicja funkcji
    if len(current_walk) == num_points: # jezeli liczba elementow listy rowna argumentowi
        walks.append(current_walk) # dopisanie do listy
    else:
        next_points = [] # pusta lista
        next_points.append(Point(curr_x - 1, curr_y)) # dopisanie do listy instancji klasy
        next_points.append(Point(curr_x + 1, curr_y)) # dopisanie do listy instancji klasy
        next_points.append(Point(curr_x, curr_y - 1)) # dopisanie do listy instancji klasy
        next_points.append(Point(curr_x, curr_y + 1)) # dopisanie do listy instancji klasy
        for point in next_points: # przechodzenie po elementach listy
            if point.x < 0: # sprawdzanie warunku
                continue # przeskok do nast iteracji
            if point.x > n: # sprawdzanie warunku
                continue # przeskok do nast iteracji
            if point.y < 0: # sprawdzanie warunku
                continue # przeskok do nast iteracji
            if point.y > n: # sprawdzanie warunku
                continue # przeskok do nast iteracji
            if visited[point.x][point.y]: # sprawdzanie warunku
                continue # przeskok do nast iteracji

            visited[point.x][point.y] = True # zmiana wartosci elementu tablicy
            current_walk.append(point) # dopisanie do listy

            find_walks(num_points, walks, current_walk, point.x, point.y, n, visited) # wywolanie funkcji

            visited[point.x][point.y] = False  # zmiana wartosci elementu tablicy
            current_walk.pop() # usuniecie ostatniego elementu tablicy 


allwalks = all_paths(5) # wywolanie funkcji z przypisaniem zwracanej wartosci do zmiennej
print(len(allwalks)) # wypisanie
