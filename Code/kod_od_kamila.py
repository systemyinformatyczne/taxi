def admin_index(request):
    pracownik = authenticate(request)
    if not pracownik:
        messages.warning(
            request,
            'Musisz się najpierw zalogować jako LABI'
        )
        return redirect('index')

    wnioski = Wniosek.objects.all().order_by('-data')
    if request.method == 'POST':
        wnioski = Wniosek.objects.all().order_by('data')
        if request.POST.get('data-nowe'):
            wnioski = Wniosek.objects.order_by('-data')
        elif request.POST.get('data-stare'):
            wnioski = Wniosek.objects.order_by('data')

        if request.POST.get('zatwierdz'):
            checked = request.POST.getlist('decyzja')
            for pk in checked:
                Historia.objects.create(
                    wniosek_id=pk,
                    status='2',
                    pracownik=pracownik.login,
                )
        elif request.POST.get('odrzuc'):
            checked = request.POST.getlist('decyzja')
            for pk in checked:
                Historia.objects.create(
                    wniosek_id=pk,
                    status='5',
                    pracownik=pracownik.login,
                )
                wniosek_mail = Wniosek.objects.get(pk=pk)
                pracownik_w = Pracownik.objects.get(pk=wniosek_mail.pracownik.pk)
                historia_w = Historia.objects.filter(wniosek=pk)
                subject = 'SODO: odrzucowno wniosek nr '+str(wniosek_mail.pk)+' w systemie'
                message = 'Odrzucono twój wniosek o numerze '+str(wniosek_mail.pk)+'.\n' \
                    'Do wiadomości dołączono raport z historią wniosku.\n' \
                    'Wiadomość wygenerowana automatycznie.'
                send_addr = wniosek_mail.pracownik.email
                email = EmailMessage(subject, message, 'sodo.uam.test@gmail.com', [send_addr])
                html = render_to_string('PDF_wnioski/wniosek_rap_pdf_wzor.html',
                                        {'wniosek': wniosek_mail, 'pracownik': pracownik_w, 'historia': historia_w})
                out = BytesIO()
                weasyprint.HTML(string=html).write_pdf(out)
                email.attach('raport_wniosek'+str(wniosek_mail.pk)+'.pdf', out.getvalue(), 'application/pdf')
                email.send()
            historia = Historia.objects.filter(wniosek=pk)
            return redirect('admin_index')

    to_approve = []
    for wniosek in wnioski:
        obiekt = wniosek.obiekty.all()[0]
        wniosek_labi = find_labi(obiekt.jedn_org.id)
        if wniosek_labi == pracownik:
            historia = Historia.objects.filter(
                wniosek=wniosek.id,
            ).order_by('-data')[0]
            if historia.status == '1':
                to_approve.append(historia)

    context = {
        'pracownik': pracownik,
        'wnioski': wnioski,
        'to_approve': to_approve,
    }
    return render(request, 'wnioski/homepage/homepage.html', context)




def gen_objs_raport_csv(request):
    obiekty = Obiekt.objects.all()
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="raport_obiekty.csv"'
    writer = csv.writer(response)
    writer.writerow(['id', 'nazwa_obiektu', 'typ_id', 'typ', 'jedn_org_id', 'jedn_org', 'opis'])
    for o in obiekty:
        writer.writerow([o.pk, o.nazwa, o.typ.id, o.typ.nazwa, o.jedn_org.id, o.jedn_org.nazwa, o.opis,])
    return response


