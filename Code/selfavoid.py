import numpy as np
import time


class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

def timing(f):
    def wrap(*args):
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        print('{:s} function took {:.3f} ms'.format(f.__name__, (time2-time1)*1000.0))

        return ret
    return wrap

@timing
def all_paths(n):
    print('Working...\n')
    walks = []
    visited = np.empty((n + 1, n + 1))
    visited.fill(False)
    num_points = (n + 1) * (n + 1)
    current_walk = []
    start = Point()
    current_walk.append(start)
    visited[0][0] = True
    find_walks(num_points, walks, current_walk, 0, 0, n, visited)
    return walks


def find_walks(num_points, walks, current_walk, curr_x, curr_y, n, visited):
    if len(current_walk) == num_points:
        walks.append(current_walk)
    else:
        next_points = []
        next_points.append(Point(curr_x - 1, curr_y))
        next_points.append(Point(curr_x + 1, curr_y))
        next_points.append(Point(curr_x, curr_y - 1))
        next_points.append(Point(curr_x, curr_y + 1))
        for point in next_points:
            if point.x < 0:
                continue
            if point.x > n:
                continue
            if point.y < 0:
                continue
            if point.y > n:
                continue
            if visited[point.x][point.y]:
                continue

            visited[point.x][point.y] = True
            current_walk.append(point)

            find_walks(num_points, walks, current_walk, point.x, point.y, n, visited)

            visited[point.x][point.y] = False
            current_walk.pop()


allwalks = all_paths(5)
print(len(allwalks))
